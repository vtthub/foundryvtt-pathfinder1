_id: 53dyu81yaw4y0nz4
_key: '!items!53dyu81yaw4y0nz4'
img: systems/pf1/icons/misc/magic-swirl.png
name: Glyph of Warding
system:
  actions:
    - _id: xito5776habiio2g
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      area: object touched or up to 5 sq. ft./level
      duration:
        dismiss: true
        units: spec
        value: permanent until discharged
      name: Object touched
      range:
        units: touch
      save:
        description: see text
        type: ref
      target:
        value: object touched or up to 5 sq. ft./level
    - _id: bn57QDqSNo7g5780
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      area: object touched or up to 5 sq. ft./level
      duration:
        dismiss: true
        units: spec
        value: permanent until discharged
      measureTemplate:
        size: 5 * @cl
        type: rect
      name: Square
      range:
        units: touch
      save:
        description: see text
        type: ref
      target:
        value: object touched or up to 5 sq. ft./level
  components:
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>This powerful inscription harms those who enter, pass, or open the
      warded area or object. A <i>Glyph of Warding</i> can guard a bridge or
      passage, ward a portal, trap a chest or box, and so on.</p><p>You set all
      of the conditions of the ward. Typically, any creature entering the warded
      area or opening the warded object without speaking a password (which you
      set when casting the spell) is subject to the magic it stores.
      Alternatively or in addition to a password trigger, glyphs can be set
      according to physical characteristics (such as height or weight) or
      creature type, subtype, or kind. Glyphs can also be set with respect to
      good, evil, law, or chaos, or to pass those of your religion. They cannot
      be set according to class, HD, or level.</p><p>Glyphs respond to invisible
      creatures normally but are not triggered by those who travel past them
      ethereally. Multiple glyphs cannot be cast on the same area. However, if a
      cabinet has three different drawers, each can be separately
      warded.</p><p>When casting the spell, you weave a tracery of faintly
      glowing lines around the warding sigil. A glyph can be placed to conform
      to any shape up to the limitations of your total square footage. When the
      spell is completed, the glyph and tracery become nearly
      invisible.</p><p>Glyphs cannot be affected or bypassed by such means as
      physical or magical probing, though they can be dispelled. <i>Mislead</i>,
      <i>polymorph</i>, and <i>nondetection</i> (and similar magical effects)
      can fool a glyph, though nonmagical disguises and the like can't. <i>Read
      magic</i> allows you to identify a <i>Glyph of Warding</i> with a DC 13
      Knowledge (arcana) check. Identifying the glyph does not discharge it and
      allows you to know the basic nature of the glyph (version, type of damage
      caused, what spell is stored).</p><p><i>Note</i>: Magic traps such as
      <i>Glyph of Warding</i> are hard to detect and disable. While any
      character can use Perception to find a glyph, only a character with the
      trapfinding class feature can use Disable Device to disarm it. The DC in
      each case is 25 + spell level, or 28 for <i>Glyph of
      Warding</i>.</p><p>Depending on the version selected, a glyph either
      blasts the intruder or activates a spell.</p><p><i>Blast Glyph</i>: A
      <i>blast glyph</i> deals 1d8 points of damage per two caster levels
      (maximum 5d8) to the intruder and to all within 5 feet of him or her. This
      damage is acid, cold, fire, electricity, or sonic (caster's choice, made
      at time of casting). Each creature affected can attempt a Reflex save to
      take half damage. Spell resistance applies against this
      effect.</p><p><i>Spell Glyph</i>: You can store any harmful spell of 3rd
      level or lower that you know. All level-dependent features of the spell
      are based on your caster level at the time of casting the glyph. If the
      spell has a target, it targets the intruder. If the spell has an area or
      an amorphous effect, the area or effect is centered on the intruder. If
      the spell summons creatures, they appear as close as possible to the
      intruder and attack.Saving Throws and spell resistance operate as normal,
      except that the DC is based on the level of the spell stored in the
      glyph.</p>
  learnedAt:
    class:
      cleric: 3
      inquisitor: 3
      oracle: 3
      warpriest: 3
      witch: 3
    domain:
      Rune: 3
    subDomain:
      Home: 3
  level: 3
  materials:
    gpValue: 200
    value: powdered diamond worth 200 gp
  school: abj
  sources:
    - id: PZO1110
      pages: '290'
  sr: false
type: spell

